import user from './module/user'
import home from './module/home'
import common from './module/common'
// 默认全部导出  import api from '@/common/vmeitime-http/'



export default {
	common,
    user,
	home
}