import Vue from 'vue'
import Vuex from 'vuex'
import user from './module/user'
import home from './module/home'
import common from './module/common'
Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    user,
	home,
	common
  }
})
