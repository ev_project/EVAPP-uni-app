import api from '@/common/http/module/user';
import jsonData from "@/Json.js"

export default {
	state: {
		hasLogin: false,
		token: null,
		userInfo: {
			id: 1,
			mobile: 18888888888,
			nickname: '您好！',
			portrait: 'http://img.61ef.cn/news/201409/28/2014092805595807.jpg'
		},
		cart: '',
		codeKey: ''
	},
	mutations: { //mutation vuex中store数据改变唯一的方法
		newCart(state, data) {
			state.cart = data
		},
		updataCodeKey(state, data) {
			state.codeKey = data
		},
		login(state, provider) {
			state.hasLogin = true;
			state.userInfo = provider;
			// console.log('login被执行！', provider, state.hasLogin)
			uni.setStorage({ //缓存用户登陆状态
				key: 'userInfo',
				data: provider
			})
			console.log(state.userInfo);
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			uni.removeStorage({
				key: 'userInfo'
			})
		},
		GetInformation(state, data) {
			state.userInfo = {
				id: 1,
				mobile: 18888888888,
				nickname: data.name,
				portrait: 'http://127.0.0.1:8000/' + data.porteait
			};
		}
	},
	getters: { //是store的计算属性 

	},

	actions: { //异步处理
		// 登录
		Login({
			state,dispatch
		}, {
			phoneno,
			password,
		}) {
			return new Promise((resolve, reject) => {
				try {
					api.login({
							username: phoneno,
							password: password,
						})
						.then(res => {
							const token = res.data.token
							if (token) {
								state.hasLogin = true
								state.token = token
								try {
									uni.setStorageSync('token', token);
									dispatch('GetInformation')
								} catch (e) {
								}
								uni.showToast({
									icon: 'none',
									title: res.data.msg
								});
								uni.navigateBack({
									delta: 1
								});
							} else {
								uni.showModal({
									content: res.data.msg,
									showCancel: false
								});
							}
							resolve(token)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//验证 验证码
		ifVerify({}, data) {
			return new Promise((resolve, reject) => {
				try {
					console.log("验证码", data)
					api.ifVerify(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		// 邮件验证
		sendEmail({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					// console.log(data)
					api.sendEmail(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//获取用户信息

		GetInformation({
			state,
			commit
		}) {
			return new Promise((resolve, reject) => {
				try {
					var time = new Date();
					console.log("获取用户数据",time);
					api.GetInformation()
						.then(res => {
							const data = res.data
							state.userInfo.nickname = data.name
							commit('GetInformation', data)
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},

		//注册
		register({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					api.register(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//修改密码
		forget({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					api.forget(data)
						.then(res => {
							const data = res.data
							uni.showToast({
								icon: 'none',
								title: data.msg
							});
							if (data.code == 2000) {
								uni.navigateBack({
									delta: 1
								});
							}
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},

		//获取购物车商品
		getPro({}) {
			return new Promise((resolve, reject) => {
				try {
					api.getPro()
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							let data = jsonData.cartList
							// console.log('getPro 接口调用了本地数据', data)
							resolve(data)
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},	
		//获取订单
		GetOrder({}) {
			return new Promise((resolve, reject) => {
				try {
					// console.log('getOrder 接', data)
					api.getUserOrder()
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							// console.log('getOrder 接口失败', data)
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//提交订单
		PostOrder({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					// console.log('getOrder 接', data)
					api.PostOrder(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							// console.log('getOrder 接口失败', data)
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//addPro 加入购物车
		addPro({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					api.addPro(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//收藏
		collection({},data) {
			return new Promise((resolve, reject) => {
				try {
					api.collection(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//购物车
		ShoppingCart({},data) {
			return new Promise((resolve, reject) => {
				try {
					console.log("接受"+data['method'])
					api.ShoppingCart(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//获取网点的接单数据
		getBranchesOrder({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					api.getBranchesOrder(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							let data = jsonData.userorderList
							// console.log('getBranchesOrder 接口调用了本地数据', data)
							resolve(data)
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		// 
		// 
	}
}
