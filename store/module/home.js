import api from '@/common/http/module/home';
import commonApi from '@/common/http/module/common';
import jsonData from "@/Json.js"
export default {
	state: {

	},
	mutations: { //mutation vuex中store数据改变唯一的方法

	},
	getters: { //是store的计算属性 

	},
	actions: { //异步处理
		// 轮番图
		getTurns({}, id) {
			return new Promise((resolve, reject) => {
				try { 
					api.getAD({
							id: 1
						})
						.then(res => {
							const data = res.data
							console.log(data)
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					// reject(err)
					let data = jsonData.carouselList
					console.log('getTurns 接口调用了本地数据', data)
					resolve(data)
				}
			})
		},
		//首页快捷通道
		getQuick({},{data}) {
			return new Promise((resolve, reject) => {
				try {
					console.log(data)
					commonApi.config(data)
						.then(res => {
							const data = res.data.config
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		//获取分类精选数据
	
		getSelection({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					commonApi.locateGoods(data)
						.then(res => {
							let tabTitle = res.data.config.config
							let listgoods = res.data.GoodsList
							
							var list = []
							var list1 = []
							for (var i in listgoods) {
								list1.push(listgoods[i])
								var a = ((i*1 + 1) % 4) == 0
								if (a) {
									list.push(list1)
									list1 = []
								}
							}
							list.push(list1)
							console.log(list)
							
							if (listgoods.length < 4) {
								list.push(list1)
							} 
							var data = {
								'tabTitle': tabTitle,
								'list': list
							}
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},

		//秒杀
		getKillGoods({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					commonApi.locateGoods(data)
						.then(res => {
							var data = {}
							data.list = res.data.GoodsList
							let time = new Date();
							let y = time.getFullYear();
							let m = time.getMonth() + 1;
							let n = time.getDate();
							let s = time.getHours() + 1;
							let f = time.getMinutes() + 1;
							let miao = time.getSeconds() + 1;
							let a = Date.UTC(y, m, n, s, f, miao);
							let b = Date.UTC(...res.data.config.config);
							data.timestamp = b - a
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},


		//热门商品下面
		getHoustBot({},{data}) {
			return new Promise((resolve, reject) => {
				try {
					commonApi.locateGoods(data)
						.then(res => {
							var data = {}
							data = res.data.config.config
							data.list = res.data.GoodsList
							resolve(data)
						}).catch(err => {
							let data = jsonData.hotRnd
							console.log('getHoustBot 接口调用了本地数据', data)
							resolve(data)
							reject(err)
						})

				} catch (error) {
					reject(error)
				}
			})
		},
		//底部热门商品 滑动加载
		HotRecommended({}, {
			data
		}) {
			return new Promise((resolve, reject) => {
				try {
					commonApi.locateGoods(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},
		

	}
}
