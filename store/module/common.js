import api from '@/common/http/module/common';
import jsonData from "@/Json.js"
import jsclass from "@/common/js/common.js"
export default {
	state: {

	},
	mutations: { //mutation vuex中store数据改变唯一的方法
		//跳转商品详情或者活动页面
		// 1 == 商品详情页
		// 2 == 站点里面的 tabBar 页面 即首页 分类 购物车 我的
		// 3 == 站点里面的 非 tabBary页面
		navToDetailPage(state, item) {
			var getData = ''
			// getData = jsclass.queryString(item.data)
			// console.log(getData)
			if (item.type == 2) {
				uni.switchTab({
					url: item.path
				});
			} else if (item.type == 3) {
				uni.navigateTo({
					url: item.path + getData
				});
			} else {
				let id = item.id;
				uni.navigateTo({
					url: `/pages/product/product?id=${id}`
				});
			}

		}
	},
	getters: { //是store的计算属性 

	},
	actions: { //异步处理
		//广告
		AD({}, data) {
			return new Promise((resolve, reject) => {
				try {
					api.getAD(data)
						.then(res => {
							const data = res.data
							// let data = jsonData.ad1;
							// console.log('广告接口数据==', data)
							resolve(data)
						}).catch(err => {
							let data = jsonData.ad1;
							// console.log('AD 接口使用本地数据 需要手动调整', '广告传递id==', id)
							resolve(data)
							reject(err)
						})
				} catch (err) {
					reject(err)
				}
			})
		},
		//页面配置信息
		config({}, data) {
			return new Promise((resolve, reject) => {
				try {
					api.config(data)
						.then(res => {
							const data = res.data.config
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (err) {
					reject(err)
				}
			})
		},
		//获取商品详情页面
		getProducts({}, data) {
			return new Promise((resolve, reject) => {
				try {
					// console.log('getProducts接受',id)
					api.getProducts(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							let data = jsonData.productData
							// console.log('getProducts 接口调用了本地数据', data)
							resolve(data)
							reject(err)
						})

				} catch (error) {
					reject(error)
				}
			})
		},
		//分类页面信息
		getEcateList({}) {
			return new Promise((resolve, reject) => {
				try {
					api.h5Class()
						.then(res => {
							let list = res.data.list
							let banner = res.data.banner
							resolve({'banner':banner,'list':list})
						}).catch(err => {
							let data = jsonData.NewecateList
							// console.log('getEcateList 接口调用了本地数据', data)
							resolve(data)
							reject(err)
						})

				} catch (error) {
					reject(error)
				}
			})
		},
		getScreening({},{data}) {
			// console.log("接受::",data)
			return new Promise((resolve, reject) => {
				try {
					api.getScreening(data)
						.then(res => {
							const data = res.data
							resolve(data)
						}).catch(err => {
							reject(err)
						})
				} catch (error) {
					reject(error)
				}
			})
		},




	}
}
