## 电动车售卖平台 小程序



### 效果预览

### 首页

![image-20220407153147563]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153147563.png)

### 完整截图

截图原因底部导航乱掉了

![完整首页截图]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/完整首页截图.png)

### 分类

![image-20220407153316721]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153316721.png)

### 网点

![image-20220407153329269]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153329269.png)

### 筛选

![image-20220407153418905]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153418905.png)

### 类型选择

![image-20220407153941919]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153941919.png)

### 购物车

![image-20220407153439842]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153439842.png)



### 订单

![image-20220407153453138]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153453138.png)

### 到店安装

![image-20220407153608022]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153608022.png)

### 收货地址

![image-20220407153505898]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153505898.png)

### 编辑收货地址

![image-20220407153522894]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153522894.png)

### 收藏列表

![image-20220407153628450]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153628450.png)

### 个人设置

![image-20220407153641229]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153641229.png)

![image-20220407153650890]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153650890.png)

### 订单状态

![image-20220407153717386]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153717386.png)

### 空页面

![image-20220407153826275]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153826275.png)

### 通知

![image-20220407153841390]( https://ev20.oss-cn-hangzhou.aliyuncs.com/GiteeImage/image-20220407153841390.png)